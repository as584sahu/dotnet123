﻿using MoviesListProject.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Linq.Dynamic;


namespace MoviesListProject.Controllers
{
    public class HomeController : Controller
    {
        MoviesEntities1 db = new MoviesEntities1();

        public ActionResult Index(int page=1, string sort="MovieName", string sortdir="asc", string search= "")
        {
            int pageSize = 10;
            int totalRecord = 0;
            if (page < 1) page = 1;
            int skip = (page * pageSize) - pageSize;
            var data = GetList(search, sort, sortdir, skip, pageSize, out totalRecord);
            ViewBag.TotalRows = totalRecord;
            ViewBag.search = search;
            return View(data);
        }

        public List<MOviesTable> GetList(string search, string sort, string sortdir,int skip, int pageSize, out int totalRecord)
        {

            using (MoviesEntities1 dc = new MoviesEntities1())
            {
                    var v = (from a in dc.MOviesTables

                             where a.MovieName.Contains(search) ||
                             a.MovieActor.Contains(search) ||
                             a.MovieDirector.Contains(search)
                             select a
                             );
                    totalRecord = v.Count();

                    v = v.OrderBy(sort + " " + sortdir);
                    if (pageSize > 0)
                    {
                        v = v.Skip(skip).Take(pageSize);
                    }


                    return v.ToList();
                
            }
        }



        //[AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        //public ActionResult Index(string searchTxt, string SortOrder, string SortBy, int PageNumber = 1)
        //{
        //    ViewBag.SortOrder = SortOrder;
        //    ViewBag.SortBy = SortBy;

        //    var users = db.MOviesTables.ToList();

        //    if (searchTxt != null)
        //    {
        //        users = db.MOviesTables.Where(x => x.MovieName.Contains(searchTxt) || x.MovieActor.Contains(searchTxt) || x.MovieDirector.Contains(searchTxt)).ToList();
        //        ApplySorting(SortOrder, SortBy, users);
        //        users = ApplyPagination(users, PageNumber);
        //    }

        //    else
        //    {
        //        ApplySorting(SortOrder, SortBy, users);
        //        users = ApplyPagination(users, PageNumber);
        //    }

        //    return View(users);
        //}

        //public void ApplySorting(string SortOrder, string SortBy, List<MOviesTable> users)
        //{
        //    switch (SortBy)
        //    {

        //        case "MovieName":
        //            {

        //                switch (SortOrder)
        //                {
        //                    case "Asc":
        //                        {
        //                            users = users.OrderBy(x => x.MovieName).ToList();
        //                            break;
        //                        }

        //                    case "Desc":
        //                        {
        //                            users = users.OrderByDescending(x => x.MovieName).ToList();
        //                            break;
        //                        }

        //                    default:
        //                        {
        //                            users = users.OrderBy(x => x.MovieName).ToList();
        //                            break;
        //                        }
        //                }
        //                break;
        //            }



        //        default:
        //            {
        //                users = users.OrderBy(x => x.MovieName).ToList();
        //                break;

        //            }
        //    }

        //}

        //public List<MOviesTable> ApplyPagination(List<MOviesTable> users,int PageNumber)
        //{
        //    ViewBag.TotalPages = Math.Ceiling(users.Count() / 5.0);
        //        ViewBag.PageNumber = PageNumber;

        //    users = users.Skip((PageNumber - 1) * 5).Take(5).ToList();
        //    return users;
        //}

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(MOviesTable movies)
        {
            try
            {
                using (MoviesEntities1 db = new MoviesEntities1())
                {
                    db.MOviesTables.Add(movies);
                    db.SaveChanges();
                }
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        public ActionResult Details(int id)
        {
           
            return View(db.MOviesTables.Where(x => x.Movieid == id).FirstOrDefault());
        }


        public ActionResult Edit(int id)
        {
            return View(db.MOviesTables.Where(x => x.Movieid == id).FirstOrDefault());
        }

        [HttpPost]
        public ActionResult Edit(int id, MOviesTable movies)
        {
            try
            {
                using (MoviesEntities1 db = new MoviesEntities1()) { 
                    db.Entry(movies).State = EntityState.Modified;
                    db.SaveChanges();
                }

                return RedirectToAction("Index");
            }

            catch 
            {
                return View();
            }
        }

        public ActionResult Delete(int id)
        {
            var e = db.MOviesTables.Where(x => x.Movieid == id).FirstOrDefault();
            db.MOviesTables.Remove(e);
            db.SaveChanges();
            return RedirectToAction("Index");
                    
   }   

    }
}












