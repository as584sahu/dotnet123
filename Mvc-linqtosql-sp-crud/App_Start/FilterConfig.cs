﻿using System.Web;
using System.Web.Mvc;

namespace Mvc_linqtosql_sp_crud
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
